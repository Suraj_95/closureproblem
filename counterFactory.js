function counterFactory() {
  let counter = 0;
  function increment() {
    return ++counter;
  }
  function decrement() {
    return --counter;
  }
  return {
    increment,
    decrement,
  };
  // return {
  //   counter: 0,

  //   increment(counter) {
  //     this.counter += 1;
  //   },

  //   decrement(counter) {
  //     this.counter -= 1;
  //   },
  // };
}

module.exports = counterFactory;
