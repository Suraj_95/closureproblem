const fxn = require("../cacheFunction");
function callBack(arguments) {
    return `${arguments} added into cached`;
}
const res = fxn(callBack);
console.log(res.called('abc'));
console.log(res.called('xyz'));
console.log(res.called(1));
console.log(res.called(undefined));
console.log(res.called(undefined));
