const counterFactory = require("../counterFactory");
const res = counterFactory();

res.increment();
res.increment();
res.increment();
res.increment();

console.log(res.increment());
res.decrement();
console.log(res.decrement());
