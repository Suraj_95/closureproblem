const limitFxn = require("../limitFunctionCallCount");
function callBack() {
  return "Hi There, How are you";
}
let n = 3;
const res = limitFxn(callBack, n);
console.log(res.called());
console.log(res.called());
console.log(res.called());
console.log(res.called());

// res.called();
// res.called();
// res.called();
// res.called();
