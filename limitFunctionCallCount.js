function limitFunctionCallCount(callBack, n) {
  function called() {
    if (n <= 0) return null;
    let exp = callBack();
    n--;
    return exp;
  }
  return { called };
}
module.exports = limitFunctionCallCount;
