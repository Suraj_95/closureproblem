function cacheFunction(cb) {
    const check =new Set();
    function called(arguments) {
        if(check.has(arguments)){
            return check;
        }else{
            check.add(arguments);
            return cb(arguments);
        }
    }
    return { called };
}
module.exports = cacheFunction;
